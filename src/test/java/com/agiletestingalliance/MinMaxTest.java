package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

@Test
public void testFoo() {
        MinMax minMax = new MinMax();
        assertEquals(minMax.foo(1,2), 2);
        assertEquals(minMax.foo(2,1), 2);
    }

@Test
public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals(minMax.bar(""), "");
    }

}
